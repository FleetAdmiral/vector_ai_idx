'''Contains functions related to MNIST Fashion dataset, including the function for class prediction'''

import torch
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import torch
import torch.nn as nn
from torch.autograd import Variable
import torchvision
import torchvision.transforms as transforms
from torch.utils.data import Dataset, DataLoader
import sys
from scipy.misc.pilutil import imread, imresize

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu") #Use GPU if available

output_mapping = {
             0: "T-shirt/Top",
             1: "Trouser",
             2: "Pullover",
             3: "Dress",
             4: "Coat",
             5: "Sandal",
             6: "Shirt",
             7: "Sneaker",
             8: "Bag",
             9: "Ankle Boot"
             }

class FashionCNN(nn.Module):
'''CNN model for MNIST fashion prediction'''

    def __init__(self):
        super(FashionCNN, self).__init__()

        #The layers of the CNN model used for MNIST prediction
        self.layer1 = nn.Sequential(
            nn.Conv2d(in_channels=1, out_channels=32, kernel_size=3, padding=1),
            nn.BatchNorm2d(32),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=2, stride=2)
        )

        self.layer2 = nn.Sequential(
            nn.Conv2d(in_channels=32, out_channels=64, kernel_size=3),
            nn.BatchNorm2d(64),
            nn.ReLU(),
            nn.MaxPool2d(2)
        )

        self.fc1 = nn.Linear(in_features=64*6*6, out_features=600)
        self.drop = nn.Dropout2d(0.25)
        self.fc2 = nn.Linear(in_features=600, out_features=120)
        self.fc3 = nn.Linear(in_features=120, out_features=10)

    def forward(self, x):
        out = self.layer1(x)
        out = self.layer2(out)
        out = out.view(out.size(0), -1)
        out = self.fc1(out)
        out = self.drop(out)
        out = self.fc2(out)
        out = self.fc3(out)
        return out


class FashionDataset(Dataset):
    '''User defined class to build a datset using Pytorch class Dataset.'''

    def __init__(self, data, transform = None):
        self.fashion_MNIST = list(data.values)
        self.transform = transform

        label = []
        image = []

        for i in self.fashion_MNIST:
            label.append(i[0])
            image.append(i[1:])
        self.labels = np.asarray(label)
        self.images = np.asarray(image).reshape(-1, 28, 28, 1).astype('float32')

    def __getitem__(self, index):
        label = self.labels[index]
        image = self.images[index]
        if self.transform is not None:
            image = self.transform(image)
        return image, label

    def __len__(self):
        return len(self.images)

def output_label(label):
    input = (label.item() if type(label) == torch.Tensor else label)
    return output_mapping[input]

def get_prediction(path): #Takes path of image as input and returns a prediction
    model = FashionCNN()
    model.load_state_dict(torch.load('pretrained_fashion.pt')) #loading pre-trained model
    if torch.cuda.is_available():
        model.cuda()

    img = imread(path, mode="L")
    img = imresize(img,(28,28))

    img = torch.from_numpy(img)
    images = img.to(device)
    test = Variable(images.view(1, 1, 28, 28))

    outputs = model(test.float()) #passes image data in 'test' through our model to get a prediction
    predictions = torch.max(outputs, 1)[1].to(device)
    predictions = predictions.cpu()
    print("Predictions is", output_mapping[predictions.numpy()[0]])
    return(output_mapping[predictions.numpy()[0]])
