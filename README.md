# MNIST Fashion image class prediction using Kafka to communicate and store data.

The file structure is fairly simple:

- ***kafka_funcs.py*** -> The python file/library with all required kafka functions for our cause.

- ***server.py*** -> The Flask server that listens for API calls and calls the appropriate kafka/PubSub function (from ***kafka_funcs.py*** and ***pubsub_funcs***)

 - ***MNIST_Server.py*** -> The file that contains the definition and methods of our Machine Learning model (which can be used to generate prediction)

 - ***pretrained_fashion.pt*** -> Our pre-trained MNIST Fashion model weights

 - ***client.py*** -> Simulates a client. Uses infinite loop to let users select a kafka function (including class prediction) and then calls corresponding API call

 - ***pubsub_funcs*** -> The python file/library with all required PubSub functions for our cause.

### Order:

1. First, run the ***server.py file*** -> your API server is now active and listening.   We use gunicorn to ensure non-blocking behaviour.  
  ``` gunicorn --workers=2 --bind=127.0.0.1:8000 server:app ```  

2. Run ***client.py*** and use menu to choose desired operation.  
  ``` python client.py ```

3. Choose your prefered option. For option 1, you need to mention the path to the file.
After getting each result, hit ENTER to reload the menu.    
The menu is of this kind:
>Pick an option:
>1) Predict Image
>2) List all topics
>3) Print list of inputs
>4) Print list of results
>
>Enter your input: 2

4. Repeat Step 2 till program quits or any server fails.

***P.S*** *: User does not have to do anything extra. Just by running client.py, the API calls, model prediction, topic creation (input and output) and the sort are done automatically by the server.*
