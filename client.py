'''This server file simulated a client connected to our service - smartphone, laptop and the kind'''

import requests

print("Pick an option amongst these two services: ")
print("1) Kafka\m2)PubSub")
choice = input("Enter your value: ")

if(choice==1):
    serv="kafka"

if(choice==2):
    serv="pubsub"

while(1): #Show a menu and return to menu after each operation
    print("Pick an option: ")
    print("1) Predict Image\n2) List all topics\n3) Print list of inputs\n4) Print list of results\n")
    val = input("Enter your value: ")

    #The program automatically makes the REST API request call based on your menu input.
    #The 'req' fields are responsible for the API calls
    if(val=='1'): # make prediction
        path = input("Give path to image: ")
        req = "http://127.0.0.1:8000/predict?filepath="
        response = requests.get(req+str(path)+"&service="+serv)
        print("Predicted class is ", response.text)
        k=input()

    if(val=='2'): # list all topics
        req = "http://127.0.0.1:8000/listtopics?service="+serv
        print("\nList of topics: ", response.text)
        k=input()

    if(val=='3'): # list of all elements in the 'input' topic
        req="http://127.0.0.1:8000/read?topicname=input&service="+serv
        response = requests.get(req)
        print("\nList of input messages: ", response.text)
        k=input()

    if(val=='4'): # list of all results
        req="http://127.0.0.1:8000/read?topicname=output&service="+serv
        response = requests.get(req)
        print("\nList of results: ", response.text)
        k=input()
