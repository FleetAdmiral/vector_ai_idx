'''All kafka functions/methods required for our use.'''

from kafka import KafkaConsumer
from kafka.admin import KafkaAdminClient, NewTopic
import sys
from kafka import KafkaProducer


def new_topics(argm): #Create a new Kafka topic
    admin_client = KafkaAdminClient(
        bootstrap_servers="localhost:9092",
        client_id='test'
    )
    topic_list = []
    topic_list.append(NewTopic(name=argm, num_partitions=1, replication_factor=1))
    admin_client.create_topics(new_topics=topic_list, validate_only=False)

def delete_topic(topic): #Delete a topic from Kafka's topic list
    topic_to_delete = topic
    admin_client = KafkaAdminClient(
        bootstrap_servers="localhost:9092",
        client_id='test'
    )
    admin_client.delete_topics([topic_to_delete])
    return("Deleted!")

def list_topics(): #List all topics active at that given point
    consumer = KafkaConsumer(group_id='test')
    consumer = consumer.topics()
    return list(consumer)

def write_message(topic_name, msg): #Write a message/event to a given topic
    producer = KafkaProducer(bootstrap_servers='localhost:9092')
    producer.send(topic_name, value=msg.encode('utf-8'))
    producer.flush()
    producer.close()

def read_messages(topic): #Read all messages/events in a topic and list them
    message_list = []
    consumer = KafkaConsumer(topic,
                             group_id=None,
                             bootstrap_servers=['localhost:9092'], auto_offset_reset='earliest', consumer_timeout_ms=10000)
    for message in consumer:
        message_list.append(message.value.decode('utf-8'))
    return message_list
