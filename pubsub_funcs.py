from google.cloud import pubsub_v1
from concurrent.futures import TimeoutError

project_id = "currtopic-1609873227244"

def new_topics_pubsub(topic_name): #Create a new pubsub topic
    topic_id=topic_name
    publisher = pubsub_v1.PublisherClient()
    topic_path = publisher.topic_path(project_id, topic_id)
    topic = publisher.create_topic(request={"name": topic_path})
    print("Created topic: {}".format(topic.name))

def delete_topic_pubsub(topic_name): #Delete a topic from PubSub's topic list
    publisher = pubsub_v1.PublisherClient()
    topic_path = publisher.topic_path(project_id, topic_name)
    publisher.delete_topic(request={"topic": topic_path})
    print("Topic deleted: {}".format(topic_path))

def list_topics_pubsub(): #List all topics active at that given point
    publisher = pubsub_v1.PublisherClient()
    project_path = f"projects/{project_id}"
    for topic in publisher.list_topics(request={"project": project_path}):
    print(topic)

def write_message_pubsub(topic_name, msg): #Write a message/event to a given topic
    publisher = pubsub_v1.PublisherClient()
    topic_path = publisher.topic_path(project_id, topic_name)
    data = msg
    data = data.encode("utf-8")
    future = publisher.publish(topic_path, data)
    print(f"Published messages to {topic_path}.")

def read_messages_pubsub(topic): #Read all messages/events in a topic and list them
    subscriber = pubsub_v1.SubscriberClient()
    subscription_path = subscriber.subscription_path(project_id, topic)
    def callback(message):
        print(message)
        message.ack()
    streaming_pull_future = subscriber.subscribe(subscription_path, callback=callback) #async subscription so that messages can be read whenever required by always listening to said topic
    with subscriber:
        try:
        streaming_pull_future.result(timeout=timeout)
    except TimeoutError:
        streaming_pull_future.cancel()
