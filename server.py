'''Central server that listens for API calls and calls the appropriate Kafka function (from kafka_funcs)'''

from flask import Flask
from flask_restful import Resource, Api, reqparse
from kafka import KafkaConsumer, KafkaProducer
from kafka.admin import KafkaAdminClient, NewTopic
import sys
import subprocess
from kafka_funcs import new_topics, write_message, read_messages, delete_topic, list_topics
from MNIST_Server import get_prediction
from google.cloud import pubsub_v1
from concurrent.futures import TimeoutError
from pubsub_funcs import new_topics_pubsub, write_message_pubsub, read_messages_pubsub, delete_topic_pubsub, list_topics_pubsub



app = Flask(__name__)
api = Api(app)
parser = reqparse.RequestParser()

#Arguments for API calls
parser.add_argument('topicname') #name of topic in consideration
parser.add_argument('message') #message/event
parser.add_argument('service') #kafka or pubsub
parser.add_argument('filepath') #path to the input file image

class Hello(Resource):
    def get(self):
        return("Application running. Please call appropriate API call.")

###################################################

class List_All_Topics(Resource):
    def get(self):
        args = parser.parse_args()

        if(args['service']=='kafka'):
            ans = list_topics()

        elif(args['service']=='pubsub'):
            ans = list_topics_pubsub()
        return ans
###################################################

class CreateTopic(Resource): #topicname, message, service
    def get(self):
        args = parser.parse_args()

        if(args['service']=='kafka'):
            new_topics(args['topicname'])

        elif(args['service']=='pubsub'):
            new_topics_pubsub(args['topicname'])

####################################################

class AddToTopic(Resource): #topicname, message, service
    def get(self):
            args = parser.parse_args()

            if(args['service']=='kafka'):
                write_message(args['topicname'], args['message'])

            if(args['service']=='pubsub'):
                write_message_pubsub(args['topicname'], args['message'])

            return('Added event to topic!')

####################################################

class ReadFromTopic(Resource): #topicname, service
    def get(self):
        args = parser.parse_args()

        if(args['service']=='kafka'):
            results = read_messages(args['topicname'])

        elif(args['service']=='pubsub'):
            results = read_messages_pubsub(args['topicname'])

        return(results)

####################################################

class DeleteTopic(Resource): #topicname, service
    def get(self):
        args = parser.parse_args()

        if(args['service']=='kafka'):
            delete_topic(args['topicname'])

        elif(args['service'=='pubsub']):
            delete_topic_pubsub(args['topicname'])

        return("Deleted!")

####################################################

class predictMNIST(Resource): #topicname, service
    def get(self):
        args = parser.parse_args()

        if(args['service']=='kafka'):
            write_message("input", args['filepath'])

        if(args['service']=='pubsub'):
            write_message_pubsub("input", args['filepath'])

        res = get_prediction(args['filepath'])

        if(args['service']=='kafka'):
            write_message("output", res)
            
        elif(args['service']=='pubsub'):
            write_message_pubsub("output", res)

        return(res)


####################################################

#Connecting API calls to appropriate function
api.add_resource(CreateTopic,'/createtopic', '/newtopic')
api.add_resource(AddToTopic, '/addtotopic', '/newtopicadd')
api.add_resource(ReadFromTopic, '/readfromtopic', '/read')
api.add_resource(DeleteTopic, '/deletetopic', '/removetopic')
api.add_resource(List_All_Topics, '/listtopics', '/listalltopics')
api.add_resource(Hello, '/')
api.add_resource(predictMNIST, '/predict')

####################################################


if __name__ == '__main__':
    app.run(debug=True)
